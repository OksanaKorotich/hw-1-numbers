package org.example;
import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Random random = new Random();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Let the game begin!");
        System.out.print("Please enter your name: ");
        String name = scanner.nextLine();
        int randomNumber = random.nextInt(101);

        int[] arrayUsersNumbers = new int[0];

        while (true) {
            System.out.print("Enter a number: ");
            if (!scanner.hasNextInt()) {
                System.out.println("Invalid input. Please enter a number.");
                scanner.next();
                continue;
            }

            int userNumber = scanner.nextInt();

            int[] newArr = new int[arrayUsersNumbers.length + 1];
            for (int i = 0; i < arrayUsersNumbers.length; i++) {
                newArr[i] = arrayUsersNumbers[i];
            }
            newArr[newArr.length - 1] = userNumber;
            arrayUsersNumbers = newArr;

            if (userNumber < randomNumber) {
                System.out.println("Your number is too small. Please, try again.");
            } else if (userNumber > randomNumber) {
                System.out.println("Your number is too big. Please, try again.");
            } else {
                System.out.println("Congratulations, " + name + "!");
                break;
            }
        }
        bubbleSort(arrayUsersNumbers);
        System.out.print("Your numbers: ");
        for (int i = 0; i < arrayUsersNumbers.length; i++) {
            System.out.print(arrayUsersNumbers[i]);
            if (i < arrayUsersNumbers.length - 1) {
                System.out.print(", ");
            }
        }
    }

    private static void bubbleSort(int[] intArray) {
        int n = intArray.length;
        int temp = 0;

        for(int i=0; i < n; i++){
            for(int j=1; j < (n-i); j++){

                if(intArray[j-1] > intArray[j]){
                    temp = intArray[j-1];
                    intArray[j-1] = intArray[j];
                    intArray[j] = temp;
                }
            }
        }
    }

}



